Rails.application.routes.draw do
  devise_for :customers, defaults: { format: :json }
  devise_for :users, defaults: { format: :json }
  resources :listings, only: [:index, :show, :update, :destroy] do
    resources :images, only: [:create, :index, :show, :update, :destroy]
  end
  resources :products, only: [:index, :show, :create, :update, :destroy] do
    get 'listings', to: 'listings#index_by_product'
  end
  resources :product_qualities, only: [:index, :show]
  resources :brands, only: [:index, :show]
  resources :contacts, only: [:show, :create, :update, :destroy]
  resources :addresses, only: [:show, :create, :update, :destroy]
  resources :companies, only: [] do
    get 'addresses', to: 'addresses#index_by_company'
    get 'contacts', to: 'contacts#index_by_company'
  end
  resources :customers, only: [:index, :show, :create, :update, :destroy] do
    resources :companies, only: [:index, :show, :create, :update, :destroy]
    resources :cards, only: [:index, :show, :create, :update, :destroy]
    resources :listings, only: [:create]
    get 'addresses', to: 'addresses#index_by_customer'
    get 'contacts', to: 'contacts#index_by_customer'
    get 'listings', to: 'listings#index_by_customer'
    resources :orders, only: [:index, :show, :create, :update, :destroy]
  end

  post 'customer/authentication', to: 'authentication#authenticate_customer'
end

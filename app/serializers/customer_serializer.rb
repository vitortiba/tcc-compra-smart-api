class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :email, :name, :birth_date, :cpf
  has_many :addresses
  has_many :contacts
  has_many :companies
end

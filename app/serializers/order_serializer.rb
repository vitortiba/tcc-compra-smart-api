class OrderSerializer < ActiveModel::Serializer
  attributes :id, :status, :amount, :message
  belongs_to :customer
  has_many :listings, serializer: ListingSerializer
  belongs_to :address
end

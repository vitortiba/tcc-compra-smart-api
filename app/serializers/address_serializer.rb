class AddressSerializer < ActiveModel::Serializer
  attributes :id, :postal_code, :number, :city, :state, :neighborhood, :complement, :street
end

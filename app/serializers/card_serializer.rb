class CardSerializer < ActiveModel::Serializer
  attributes :id, :masked_number, :expiration_month, :expiration_year, :name
  belongs_to :customer
end

class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :trade_name, :cnpj
  belongs_to :customer
  has_many :addresses
  has_many :contacts
end

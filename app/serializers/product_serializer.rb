class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :brand, :description, :launch_date, :minimum_price, :maximum_price, :product_type, :status, :brand_id
  belongs_to :brand
end

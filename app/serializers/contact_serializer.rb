class ContactSerializer < ActiveModel::Serializer
  attributes :id, :number, :area_code
  belongs_to :customer
  belongs_to :company
end

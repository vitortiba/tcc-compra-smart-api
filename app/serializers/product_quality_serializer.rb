class ProductQualitySerializer < ActiveModel::Serializer
  attributes :id, :tag, :name, :description
end

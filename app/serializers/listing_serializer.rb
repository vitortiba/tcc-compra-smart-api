class ListingSerializer < ActiveModel::Serializer
  attributes :id, :rating, :description, :price, :discount, :buy_date, :order_id
  belongs_to :customer
  belongs_to :product, serializer: ProductSerializer
  belongs_to :product_quality
  has_many :images
end

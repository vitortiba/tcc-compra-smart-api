class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]
  before_action :set_customer, only: [:index_by_customer]
  before_action :set_company, only: [:index_by_company]

  def index_by_customer
    @addresses = @customer.addresses
    render json: @addresses
  end

  def index_by_company
    @addresses = @company.addresses
    render json: @addresses
  end

  def show
    render json: @address
  end

  def create
    @address = Address.create!(address_params)
    render json: @address
  end

  def update
    @address.update_attributes!(address_params)
    render json: @address
  end

  def destroy
    @address.destroy!
    head :ok
  end

  private
    def set_company
      @company = Company.find(params[:company_id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_address
      @address = Address.find(params[:id])
    end

    def address_params
      params.require(:address).permit(:postal_code, :number, :city, :state, :neighborhood, :complement, :street, :customer_id, :company_id)
    end
end

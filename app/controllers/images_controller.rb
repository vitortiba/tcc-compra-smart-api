require "#{Rails.root}/lib/aws_file"

class ImagesController < ApplicationController
  before_action :set_listing, only: [:create, :index, :show, :update, :destroy]
  before_action :set_image, only: [:show, :update, :destroy]

  def index
    @images = @listing.images
    render json: @images
  end

  def show
    render json: @image
  end

  def create
    @image = @listing.images.create!(url: AwsFile.new.upload_image(params[:file], 'listing_images'))
    render json: @image
  end

  def update
    @image.update_attributes!(url: AwsFile.new.upload_image(params[:file], 'listing_images'))
    render json: @image
  end

  def destroy
    @image.destroy!
    head :ok
  end

  private
    def set_listing
      @listing = Listing.find(params[:listing_id])
    end

    def set_image
      @image = @listing.images.find(params[:id])
    end
end

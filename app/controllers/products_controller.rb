class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  def index
    @products = Product.all
    render json: @products
  end

  def show
    render json: @product
  end

  def create
    @product = Product.create!(product_params)
    render json: @product
  end

  def update
    @product.update_attributes!(product_params)
    render json: @product
  end

  def destroy
    @product.destroy!
    head :ok
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :description, :launch_date, :minimum_price, :maximum_price, :product_type, :status, :brand_id, :product_quality_id)
    end
end

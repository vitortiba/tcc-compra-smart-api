class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :update, :destroy]
  before_action :authenticate_customer!

  def index
    @customers = Customer.all
    render json: @customers
  end

  def show
    render json: @customer
  end

  def create
    @customer = Customer.create!(customer_params)
    render json: @customer
  end

  def update
    @customer.update_attributes(customer_params)
    render json: @customer
  end

  def destroy
    @customer.destroy!
    head :ok
  end

  private
    def set_customer
      @customer = Customer.find(params[:id])
    end

    def customer_params
      params.require(:customer).permit(:name, :birth_date, :cpf, :kind, :email, :password)
    end
end

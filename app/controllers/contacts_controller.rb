class ContactsController < ApplicationController
  before_action :set_company, only: [:index_by_company]
  before_action :set_customer, only: [:index_by_customer]
  before_action :set_contact, only: [:show, :update, :destroy]

  def index_by_customer
    @contacts = @customer.contacts
    render json: @contacts
  end

  def index_by_company
    @contacts = @company.contacts
    render json: @contacts
  end

  def show
    render json: @contact
  end

  def create
    @contact = Contact.create!(contact_params)
    render json: @contact    
  end

  def update
    @contact.update_attributes!(contact_params)
    render json: @contact
  end

  def destroy
    @contact.destroy!
    head :ok
  end

  private
    def set_company
      @company = Company.find(params[:company_id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_contact
      @contact = Contact.find(params[:id])
    end

    def contact_params
      params.require(:contact).permit(:number, :area_code, :customer_id, :company_id)
    end
end

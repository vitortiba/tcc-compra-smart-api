class OrdersController < ApplicationController
  before_action :set_customer, only: [:create, :index, :show, :update, :destroy]
  before_action :set_order, only: [:show, :update, :destroy]
  before_action :authenticate_customer!

  def index
    @orders = @customer.orders
    render json: @orders
  end

  def show
    render json: @order
  end

  def create
    ActiveRecord::Base.transaction do
      @order = @customer.orders.create!(order_params)
      Listing.where(id: params[:order]['listing_ids']).map do |listing|
        listing.update_attributes!(order_id: @order.id)
      end
    end
    render json: @order
  end

  def update
    @order.update_attributes(order_params)
    render json: @order
  end

  def destroy
    @order.destroy!
    head :ok
  end

  private
    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_order
      @order = @listing.orders.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:customer_id, :address_id, :status, :message, :amount)
    end
end

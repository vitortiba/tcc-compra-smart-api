class CardsController < ApplicationController
  before_action :set_customer, only: [:create, :index, :show, :update, :destroy]
  before_action :set_card, only: [:show, :update, :destroy]

  def index
    @cards = @customer.cards
    render json: @cards
  end

  def show
    render json: @card
  end

  def create
    @card = @customer.cards.create!(card_params)
    render json: @card
  end

  def update
    @card = @card.update_attributes!(card_params)
    render json: @card
  end

  def destroy
    @card.destroy!
    head :ok
  end

  private
    def set_card
      @card = @customer.cards.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def card_params
      params.require(:card).permit(:masked_number, :expiration_month, :expiration_year, :name)
    end
end

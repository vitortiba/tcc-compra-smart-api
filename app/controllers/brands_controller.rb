class BrandsController < ApplicationController
  before_action :set_brand, only: [:show]

  def index
    @brands = Brand.all
    render json: @brands
  end

  def show
    render json: @brand
  end

  private

    def set_brand
      @brand = Brand.find(params[:id])
    end
end

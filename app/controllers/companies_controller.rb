class CompaniesController < ApplicationController
  before_action :set_customer, only: [:create, :index, :show, :update, :destroy]
  before_action :set_company, only: [:show, :update, :destroy]

  def index
    @companies = @customer.companies
    render json: @companies
  end

  def show
    render json: @company
  end

  def create
    @company = @customer.companies.create!(company_params)
    render json: @company
  end

  def update
    @company.update_attributes!(company_params)
    render json: @company
  end

  def destroy
    @company.destroy!
    head :ok
  end

  private
    def set_company
      @company = @customer.companies.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def company_params
      params.require(:company).permit(:name, :trade_name, :cnpj)
    end
end

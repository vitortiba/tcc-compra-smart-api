class ListingsController < ApplicationController
  before_action :set_customer, only: [:create, :index_by_customer]
  before_action :set_product, only: [:index_by_product]
  before_action :set_listing, only: [:show, :update, :destroy]
  before_action :set_filters, only: [:index]
  before_action :authenticate_customer!, only: [:update, :create]

  def index
    @listings = Listing.ransack(@filters).result
    render json: @listings
  end

  def index_by_product
    @listings = @product.listings
    render json: @listings
  end

  def index_by_customer
    @listings = @customer.listings
    render json: @listings
  end

  def show
    render json: @listing
  end

  def create
    @listing = @customer.listings.create!(listing_params)
    render json: @listing
  end

  def update
    @listing.update_attributes!(listing_params)
    render json: @listing
  end

  def destroy
    @listing.destroy!
    head :ok
  end

  private
    def set_filters
      @filters = {
        product_name_cont: params[:product_name],
        product_id_eq: params[:product_id],
        product_brand_id_eq: params[:brand_id],
        product_quality_tag_eq: params[:quality_tag]
      }
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_product
      @product = Product.find(params[:product_id])
    end

    def set_listing
      @listing = Listing.find(params[:id])
    end

    def listing_params
      params.require(:listing).permit(:description, :price, :product_id, :product_quality_id, :buy_date)
    end
end

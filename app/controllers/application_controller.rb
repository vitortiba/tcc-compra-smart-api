require 'json_token'

class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  attr_reader :current_customer

  def authenticate_customer!
    decode_token
    set_current
  rescue
    not_authenticated
  end

  def decode_token
    request_token = request.headers['Authorization']
    @decripted_token ||= JsonToken.decode(request_token)
    @decripted_token
  end

  def set_current
    @current_customer = Customer.find(@decripted_token[:customer_id])
    raise if @current_customer.id != @decripted_token[:customer_id] || @decripted_token[:key] != JsonToken::PUBLIC_KEY
  end

  def not_authenticated
    render json: { error: 'Não autenticado.' }, status: :unauthorized
  end
end

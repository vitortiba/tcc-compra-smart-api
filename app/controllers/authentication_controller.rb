require 'json_token'

class AuthenticationController < ApplicationController

  def authenticate_customer
    customer = Customer.find_for_database_authentication(email: params[:email])
    if customer && customer.valid_password?(params[:password])
      sign_in(customer)
      token = authentication_token(customer)
      render json: response_customer(customer, token)
    else
      head :unauthorized
    end
  end

  def response_customer(customer, token)
    {
      auth_token: token,
      customer: {
        id: customer.id,
        name: customer.name,
        email: customer.email,
        created_at: customer.created_at.utc.to_i
      }
    }
  end

  def authentication_token(customer)
    JsonToken.encode(customer_id: customer.id, key: JsonToken::PUBLIC_KEY)
  end
end

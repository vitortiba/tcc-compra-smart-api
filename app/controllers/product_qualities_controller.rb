class ProductQualitiesController < ApplicationController
  before_action :set_product_quality, only: [:show]

  def index
    @product_qualities = ProductQuality.all
    render json: @product_qualities
  end

  def show
    render json: @product_quality
  end

  private
    def set_product_quality
      @product_quality = ProductQuality.find(params[:id])
    end
end

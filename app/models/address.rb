class Address < ApplicationRecord
  belongs_to :customer
  belongs_to :company, optional: true
  has_many :orders
end

class Customer < ApplicationRecord

  devise :database_authenticatable,
         :recoverable,
         :validatable

  has_many :addresses
  has_many :contacts
  has_many :companies
  has_many :listings	
  has_many :orders
  has_many :cards
end

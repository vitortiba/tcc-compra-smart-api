class Order < ApplicationRecord
  belongs_to :customer
  has_many :listings
  belongs_to :address
end

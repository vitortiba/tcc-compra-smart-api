class Listing < ApplicationRecord
  belongs_to :customer
  belongs_to :product
  belongs_to :product_quality
  has_many :images
end

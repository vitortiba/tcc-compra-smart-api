class Company < ApplicationRecord
  belongs_to :customer
  has_many :addresses
  has_many :contacts
end

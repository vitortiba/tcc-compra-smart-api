require 'aws-sdk-s3'

class AwsFile
  CDN_URL = 'https://compra-smart.s3.us-east-2.amazonaws.com/'

  def initialize(bucket = 'compra-smart', region = 'us-east-2')
    @bucket = bucket
    @region = region
  end

  def upload_image(file, bucket_path)
    validate_type_image!(file)
    s3 = Aws::S3::Client.new(region: @region, credentials: credentials)

    file_name = file.original_filename
    key = "#{bucket_path}/#{file_name}"

    s3.put_object(body: file.tempfile, bucket: @bucket, key: key)
    CDN_URL + key
  end

  private

  def validate_type_image!(file)
    unless %w[png jpeg jpg].include?(file.content_type.split('/').last.downcase)
      raise 'Tipo de arquivo inválido ou não enviado.'
    end 
  end

  def credentials
    Aws::Credentials.new(Rails.application.credentials.config[:AWS_ACCESS_KEY_ID], 
                         Rails.application.credentials.config[:AWS_SECRET_ACCESS_KEY])
  end
end
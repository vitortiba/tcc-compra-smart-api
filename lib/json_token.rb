class JsonToken

  ENCODE_KEY = '72e19753f4a85d6e9b86bbe6f535265ea0c9f77d7b72e5655f87cc80279796f5'
  PUBLIC_KEY = '5bd68b4023c524856c12efaa37375a0a'

  def self.encode(data)
    data = data.dup
    # data['exp'] = expiration.to_i
    JWT.encode(data, ENCODE_KEY)
  end

  def self.decode(token)
    HashWithIndifferentAccess.new(JWT.decode(token, ENCODE_KEY)[0])
  end
end

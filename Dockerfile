FROM ruby:2.5
LABEL maintainer="Vitor Hideki Yamamoto Tiba <vitor.hitiba@gmail.com>"

RUN apt-get install -y curl
RUN apt-get update
RUN apt-get install -y nano
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -E -
RUN apt-get update -qq && apt-get install -y nodejs
RUN mkdir /tcc-compra-smart-api
WORKDIR /tcc-compra-smart-api
COPY Gemfile /tcc-compra-smart-api/Gemfile
COPY Gemfile.lock /tcc-compra-smart-api/Gemfile.lock
RUN bundle install
COPY . /tcc-compra-smart-api

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

RUN useradd -ms /bin/bash docker
RUN chown -Rf docker:docker .
RUN chmod -R 777 .
USER docker

CMD ["bundle", "exec", "puma", "-p", "3000"]

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_29_185548) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "postal_code"
    t.string "number"
    t.string "city"
    t.string "state"
    t.string "neighborhood"
    t.string "complement"
    t.string "street"
    t.bigint "customer_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_addresses_on_company_id"
    t.index ["customer_id"], name: "index_addresses_on_customer_id"
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards", force: :cascade do |t|
    t.string "masked_number"
    t.integer "expiration_month"
    t.string "expiration_year"
    t.string "name"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_cards_on_customer_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "trade_name"
    t.string "cnpj"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_companies_on_customer_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "number"
    t.string "area_code"
    t.bigint "customer_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_contacts_on_company_id"
    t.index ["customer_id"], name: "index_contacts_on_customer_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.date "birth_date"
    t.string "cpf"
    t.string "kind", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true
  end

  create_table "images", force: :cascade do |t|
    t.string "url"
    t.bigint "listing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["listing_id"], name: "index_images_on_listing_id"
  end

  create_table "listings", force: :cascade do |t|
    t.integer "rating"
    t.text "description"
    t.float "price"
    t.bigint "customer_id"
    t.bigint "product_id"
    t.bigint "product_quality_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "discount"
    t.date "buy_date"
    t.bigint "order_id"
    t.index ["customer_id"], name: "index_listings_on_customer_id"
    t.index ["order_id"], name: "index_listings_on_order_id"
    t.index ["product_id"], name: "index_listings_on_product_id"
    t.index ["product_quality_id"], name: "index_listings_on_product_quality_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "address_id"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "amount"
    t.string "message"
    t.index ["address_id"], name: "index_orders_on_address_id"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
  end

  create_table "product_qualities", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tag"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.date "launch_date"
    t.float "minimum_price"
    t.float "maximum_price"
    t.integer "product_type"
    t.integer "status"
    t.bigint "brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_products_on_brand_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "addresses", "companies"
  add_foreign_key "addresses", "customers"
  add_foreign_key "cards", "customers"
  add_foreign_key "companies", "customers"
  add_foreign_key "contacts", "companies"
  add_foreign_key "contacts", "customers"
  add_foreign_key "images", "listings"
  add_foreign_key "listings", "customers"
  add_foreign_key "listings", "orders"
  add_foreign_key "listings", "product_qualities"
  add_foreign_key "listings", "products"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "customers"
  add_foreign_key "products", "brands"
end

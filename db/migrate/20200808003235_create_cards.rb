class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :masked_number
      t.integer :expiration_month
      t.string :expiration_year
      t.string :name
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end

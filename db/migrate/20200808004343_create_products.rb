class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.date :launch_date
      t.float :minimum_price
      t.float :maximum_price
      t.integer :product_type
      t.integer :status
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end

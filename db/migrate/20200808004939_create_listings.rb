class CreateListings < ActiveRecord::Migration[5.2]
  def change
    create_table :listings do |t|
      t.integer :rating
      t.text :description
      t.float :price
      t.references :customer, foreign_key: true
      t.references :product, foreign_key: true
      t.references :product_quality, foreign_key: true

      t.timestamps
    end
  end
end

class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.date :birth_date
      t.string :cpf
      t.string :kind, limit: 1

      t.timestamps
    end
  end
end

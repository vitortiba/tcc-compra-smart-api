class AddDiscountColumnToListings < ActiveRecord::Migration[5.2]
  def change
	add_column :listings, :discount, :float
  end
end

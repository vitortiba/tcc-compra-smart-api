class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.references :listing, foreign_key: true
      t.references :address, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end

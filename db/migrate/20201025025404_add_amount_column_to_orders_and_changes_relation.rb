class AddAmountColumnToOrdersAndChangesRelation < ActiveRecord::Migration[5.2]
  def change
  	add_column :orders, :amount, :float
  	add_reference :listings, :order, foreign_key: true
  	remove_reference :orders, :listing
  end
end

class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :postal_code
      t.string :number
      t.string :city
      t.string :state
      t.string :neighborhood
      t.string :complement
      t.string :street
      t.references :customer, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end

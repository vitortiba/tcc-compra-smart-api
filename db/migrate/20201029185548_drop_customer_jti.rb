class DropCustomerJti < ActiveRecord::Migration[5.2]
  def change
    remove_column :customers, :jti
  end
end

class AddBuyDateColumnToListings < ActiveRecord::Migration[5.2]
  def change
	add_column :listings, :buy_date, :date  	
  end
end

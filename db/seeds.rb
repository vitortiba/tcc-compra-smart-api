# Qualidades de um anúncio
ProductQuality.find_or_create_by!(id: 1, tag: 'brand_new', name: 'Novo', description: 'Novo de fábrica sem nenhum defeito e sem ser utilizado.')
ProductQuality.find_or_create_by!(id: 2, tag: 'good', name: 'Ótimo', description: 'Com pouquíssimas marcas de uso, com no máximo pequenos arranhões e 3 meses de uso')
ProductQuality.find_or_create_by!(id: 3, tag: 'normally_used', name: 'Com marcas de uso', description: 'Com marcas de uso normais, com pequenos riscos e no máximo 12 meses de uso')
ProductQuality.find_or_create_by!(id: 4, tag: 'damaged_usable', name: 'Funcional com danos significativos', description: 'Funcional, porém com danos significativos (um trinco na tela, por exemplo).')
ProductQuality.find_or_create_by!(id: 5, tag: 'damaged', name: 'Danificado', description: 'Danificado, onde alguma parte não funciona e deve ser informada.')

# Marcas de celulares
Brand.find_or_create_by!(id: 1, name: 'Samsung')
Brand.find_or_create_by!(id: 2, name: 'Apple')
Brand.find_or_create_by!(id: 3, name: 'Motorola')
Brand.find_or_create_by!(id: 4, name: 'LG')
Brand.find_or_create_by!(id: 5, name: 'Xiaomi')
Brand.find_or_create_by!(id: 6, name: 'Asus')

# Produtos
# SAMSUNG
Product.find_or_create_by!(name: 'S10', 
	              description: 'Smartphone Samsung Galaxy S10 128GB Azul 4G - 8GB RAM 6,1” Câm. Tripla + Câm. Selfie 10MP. O Samsung Galaxy S10 azul é o Smartphone que não apenas se destaca, mas se diferencia. Ele conta com tela de 6,1", 128GB de armazenamento interno, 8GB de RAM, processador Octa-Core, tecnologia 3G/4G, câmera traseira tripla com 12MP OIS (F1.5/F2.4) + Ultra Wide 16MP (F2.2) + 12MP OIS (F2.4) e flash LED, câmera selfie de 10MP Auto Focus com flash na tela e é Dual Chip com o segundo slot hibrido, podendo ser utilizado para o cartão de memória (MicroSD) ou chip da operadora. O display Infinity-O permite uma visualização que realmente não tem ininterrupções. Utilizando corte a laser preciso, a câmera foi discretamente escondida dentro do display sem sacrificar a qualidade da foto. O desbloqueio Ultrassônico é invisível e tão seguro quanto um cofre. O leitor de impressão digital foi movido de trás para a frente, criando um revolucionário sensor na tela. Utilizando pulsos ultrassônicos, ele detecta os sulcos e vales 3D de sua impressão digital, para que apenas você possa acessar seu telefone. É seguro e prático, além de permitir que você desbloqueie, arraste e segure para abrir o aplicativo desejado.A tela Dynamic AMOLED tem certificado HDR10+, oferecendo o Dynamic Tone Mapping para cores e contrastes incrivelmente reais em cada cena, até mesmo as escuras. Além disso, o modo de economia de energia adaptável gerencia a duração da sua bateria com base na previsão do seu dia. ',
	   						minimum_price: 2000,
	   						maximum_price: 3000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Note 10 Lite', 
	   						description: 'Descubra todas as possibilidades com o Samsung Galaxy Note 10 Lite, que oferece alto desempenho, tela cinematográfica, câmeras com tecnologia de ponta, design incrivelmente fino e uma caneta inteligente, tudo para lhe proporcionar a melhor experiência móvel em um único aparelho. O Galaxy Note 10 Lite têm um sistema de câmera tripla para garantir fotos com qualidade profissional. Enquadre o que importa com a câmera Wide de 12MP. Para capturar cenas mais abertas e não deixar nada de fora uma lente Ultra Wide de 12MP com campo de visão de 123 graus. Já para fotografar algo distante, uma câmera Teleobjetiva de 12MP com zoom óptico de 2x e estabilização óptica de imagem vai aproximar o que quer que você esteja tentando captar. O desempenho do Galaxy Note 10 Lite é incrível, ele combina um poderoso processador com uma memória RAM de 6 GB, isso permite fazer transmissões ao vivo, jogar, fazer downloads ou várias outras tarefas em alta velocidade sem travamentos. Sua robusta bateria, com capacidade de 4.500 mAh, garante o uso do celular por longos períodos sem precisar de carga. O Samsung Galaxy Note 10 Lite traz recursos avançados. A caneta SPen, além de poder anotar na tela suas ideias, fazer anotações de projeto, copiar, colar e compartilhar tudo rapidamente, ela possui suporte a Bluetooth, agindo como um controle remoto. É só apertar a S Pen uma ou duas vezes para tirar fotos facilmente sem precisar tocar na tela do celular, navegar por sua galeria de fotos, ou reproduzir e pausar suas músicas. A alta capacidade de armazenamento do Galaxy Note 10 Lite possibilita ao usuário guardar suas músicas favoritas, fotos e filmes sem problema de espaço. São 128 GB de memória interna, com a opção de memória expansível de até 1 TB com cartão MicroSD (vendido separadamente). ',
	   						minimum_price: 1500,
	   						maximum_price: 3000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Note 10+', 
	   						description: 'Smartphone Samsung Galaxy Note 10+ 256GB Dual Chip Android 9.0 Tela 6.8” Octa-Core 4G Câmera 2MP + 16MP (Ultra Wide) + 12MP + Tof (Scanner 3D). O armazenamento real disponível pode variar dependendo do software pré-instalado. Expansão MicroSD disponível apenas no Galaxy Note10+ e Galaxy Note10+ 5G. Cartão MicroSD vendido separadamente. A disponibilidade do cartão MicroSD pode variar dependendo do país e do fabricante. Estimado tendo como base o perfil de uso de um usuário médio/comum. Avaliado de forma independente pela Strategy Analytics entre 17 de julho e 2 de agosto de 2019 nos Estados Unidos e Reino Unido com as versões de pré-lançamento de SM N970 e SM N975. A duração real da bateria varia conforme o ambiente de rede, recursos e aplicativos usados, frequência de chamadas e mensagens, número de cargas e muitos outros fatores. O valor padrão testado em condições de laboratório terceirizado. O valor padrão é o valor médio estimado considerando o desvio na capacidade da bateria entre as amostras de bateria testadas sob o padrão IEC 61960. A capacidade nominal (mínima) é de 3.400 mAh para o Galaxy Note10 e 4.170 mAh para o Galaxy Note10+. A duração real da bateria pode variar dependendo do ambiente de rede, padrões de uso e outros fatores. Foto tirada com o Galaxy Note10+. Vídeo gravado com o Galaxy Note10+. Recurso de profundidade de campo disponível apenas no Galaxy Note10+. O vídeo de foco ao vivo no Galaxy Note10+ usa a Câmera DepthVision e a Câmera grande angular. No Galaxy Note10, o vídeo com foco ao vivo é filmado utilizando inteligência artificial e pode ter resultados variados devido à diferença de tecnologia. Vídeo gravado com o Galaxy Note10+. Vídeo gravado com o Galaxy Note10+. 5 câmeras disponíveis apenas no Galaxy Note10+ (1 frontal e 4 traseiras). O Galaxy Note 10 possui 4 câmeras (1 frontal e 3 traseiras). Fotos tiradas com o Galaxy Note10+. A função de exportação de texto real pode exigir algum processamento para transformar a escrita em texto. A disponibilidade do recurso de exportação de texto pode variar conforme o idioma. O suporte de ações aéreas pode variar conforme o aplicativo. ',
	   						minimum_price: 2500,
	   						maximum_price: 4000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Note 10', 
    description: 'De um tamanho de tela espetacular até um espaço intuitivo para a S Pen, o Galaxy Note10 Lite tem design e desempenho incríveis. Sua estrutura suave de metal se curva elegantemente, oferecendo conforto e sensação ergonômicos. Com Display Infinito Full HD+ de 6.7” e tecnologia Super AMOLED, para você poder jogar, assistir e navegar nos seus conteúdos favoritos sem interrupções. Um leitor de impressão digital é embutido diretamente na tela, oferecendo uma camada invisível de segurança como a de um cofre. A S Pen é a varinha mágica do seu smartphone. Com ela você tem acesso a todo o poder do seu Galaxy Note10 Lite. Seja para criar um desenho inspirador ou fazer uma anotação quando necessário. A S Pen liberta todo seu potencial criativo e produtivo com velocidade, eficiência e diversão. Com uma estrutura única e simplificada, uma ponta extremamente fina e diversos níveis de pressão, a S Pen é precisa e suave como escrever com papel e caneta. Habilitada por Bluetooth, a S Pen também age como um controle remoto para o seu Galaxy Note10 Lite. É só apertar a caneta uma ou duas vezes, para tirar fotos facilmente sem precisar tocar na tela do celular, navegar por sua galeria de fotos, ou reproduzir e pausar suas músicas. Com uma super bateria de 4500mAh, o Galaxy Note10 Lite permite que você aproveite seu aparelho o dia todo, com apenas uma hora de recarga. Graças a inteligência artificial do aparelho, que reconhece e se ajusta ao seu padrão de uso do celular. Além de tudo isso, o Galaxy Note10 Lite ainda conta com um poderoso sistema multicâmeras. Em sua traseira, ele conta com uma Câmera Ultra Wide que expande seu campo de visão para 123°, uma Câmera Wide de 12MP que permite tirar lindas fotos de perto, e por fim, uma Câmera Teleobjetiva, também de 12MP, que possui estabilização óptica de imagem.',
    minimum_price: 2500,
    maximum_price: 3500,
    product_type: 1,
    status: 1,
    brand_id: 1)
Product.find_or_create_by!(name: 'S10+', 
	   						description: 'Resultado de 10 anos de inovações pioneiras em smartphones, o Galaxy S10, introduz a próxima geração de smartphones, em uma experiência perfeita em todos os sentidos!. Design e acessibilidade. Display Cinematográfico que é ver para crer. A tela Dynamic AMOLED tem certificado HDR10+, oferecendo o Dynamic Tone Mapping para cores e contrastes incrivelmente reais em cada cena, até mesmo as escuras. E a tela mais clara ajuda você a ver perfeitamente mesmo sob luz do dia. Junto com alto-falantes estéreo e Dolby Atmos, a experiência é realmente imersiva. Display Eye Comfort projetado para se ajustar à luz para uma melhor visualização. Um composto de fluido exclusivo no Dynamic AMOLED reduz a luz azul prejudicial sem alterar a cor do que está na tela. Assista ou jogue até tarde da noite com menos cansaço visual para uma noite de sono melhor. Desbloqueio Ultrassônico é invisível e tão seguro quanto um cofre. Nós movemos o leitor de impressão digital de trás para a frente, criando um revolucionário sensor na tela. Utilizando pulsos ultrassônicos, ele detecta os sulcos e vales 3D de sua impressão digital, para que apenas você possa acessar seu telefone. É seguro e prático, além de permitir que você desbloqueie, arraste e segure para abrir o aplicativo desejado. ',
	   						minimum_price: 2500,
	   						maximum_price: 4000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'S9', 
	   						description: 'O Samsung Galaxy S9 é um dos smartphones Android mais avançados e completos que existem em circulação. Tem um grande display de 5.8 polegadas e uma resolução de 2960x1440 pixels que é uma das mais altas atualmente em circulação. As funcionalidades oferecidas pelo Samsung Galaxy S9 são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 128 GB com a possibilidade de expansão. Câmera discreta de 12 megapixel mas que permite ao Samsung Galaxy S9 tirar fotos de boa qualidade com uma resolução de 4032x3024 pixel e gravar vídeos em 4K a espantosa resolução de 3840x2160 pixels. ',
	   						minimum_price: 1000,
	   						maximum_price: 2000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Note 9', 
	   						description: 'O Samsung Galaxy Note 9 é um dos smartphones Android mais avançados e completos que existem em circulação. Tem um grande display de 6.4 polegadas e uma resolução de 2960x1440 pixels que é uma das mais altas atualmente em circulação. As funcionalidades oferecidas pelo Samsung Galaxy Note 9 são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 128 GB com a possibilidade de expansão. Câmera discreta de 12 megapixel mas que permite ao Samsung Galaxy Note 9 tirar fotos de boa qualidade com uma resolução de 4290x2800 pixel e gravar vídeos em 4K a espantosa resolução de 3840x2160 pixels.',
	   						minimum_price: 1300,
	   						maximum_price: 2000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Galaxy A31', 
	   						description: 'O Samsung Galaxy A31 é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.4 polegadas com uma resolução de 2400x1080 pixel. Sobre as características deste Samsung Galaxy A31 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 128 GB com a possibilidade de expansão. ',
	   						minimum_price: 1000,
	   						maximum_price: 2000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Galaxy A10', 
	   						description: 'O Samsung Galaxy A10 é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Surpreendente é sua tela Touchscreen de 6.2 polegadas, que coloca esse Samsung no topo de sua categoria. A resolução também é alta: 1520x720 pixel. Quanto às funções, no Samsung Galaxy A10 realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão.',
	   						minimum_price: 500,
	   						maximum_price: 1000,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)
Product.find_or_create_by!(name: 'Galaxy J6 Plus', 
	   						description: 'O Samsung Galaxy J6 Plus é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Surpreendente é sua tela Touchscreen de 6 polegadas, que coloca esse Samsung no topo de sua categoria. A resolução também é alta: 1480x720 pixel. Quanto às funções, no Samsung Galaxy J6 Plus realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
	   						minimum_price: 1500,
	   						maximum_price: 2500,
	   						product_type: 1,
	   						status: 1,
	   						brand_id: 1)

# APPLE
Product.find_or_create_by!(name: 'Iphone 11 pro max', 
    description: 'Revolucionário sistema de câmera tripla que amplia possibilidades sem abrir mão da simplicidade. Um salto sem precedentes em duração de bateria. Chip assustadoramente potente que melhora ainda mais o aprendizado de máquina e redefine o que um smartphone pode fazer. Conheça o primeiro iPhone poderoso o suficiente para ser chamado de Pro. Conheca o primeiro sistema de camera tripla que combina tecnología de ponta com a já conhecida simplicidade do iPhone.',
    minimum_price: 4000,
    maximum_price: 5000,
    product_type: 1,
    status: 1,
    brand_id: 2)

Product.find_or_create_by!(name: 'Iphone 11', 
    description: 'O iPhone 11 tem novo sistema de câmera dupla para capturar mais do que você vê e ama. Ele vem com o chip mais rápido em um smartphone. E bateria para o dia todo: você faz muito e recarrega pouco. Também tem a melhor qualidade de vídeo em um celular. Assim, suas memórias ficam inesquecíveis de verdade. Ambas as câmeras do iPhone 11 gravam vídeos 4K nítidos a 60 qps.',
    minimum_price: 3000,
    maximum_price: 4500,
    product_type: 1,
    status: 1,
    brand_id: 2)

Product.find_or_create_by!(name: 'Iphone SE', 
    description: 'O iPhone SE chegou equipado com o nosso chip mais poderoso e com aquele tamanho compacto que encaixa na sua mão. Exatamente como você queria.',
    minimum_price: 2500,
    maximum_price: 4000,
    product_type: 1,
    status: 1,
    brand_id: 2)

Product.find_or_create_by!(name: 'Iphone 8 Plus', 
    description: 'O Apple iPhone 8 Plus é um dos smartphones iOS mais avançados e completos que existem em circulação. Tem um grande display de 5.5 polegadas com uma resolução de 1920x1080 pixel. As funcionalidades oferecidas pelo Apple iPhone 8 Plus são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 256 GB mas sem a possibilidade de expansão. ',
    minimum_price: 3000,
    maximum_price: 4000,
    product_type: 1,
    status: 1,
    brand_id: 2)

Product.find_or_create_by!(name: 'Iphone X', 
    description: 'O Apple iPhone X é um dos smartphones iOS mais avançados e completos que existem em circulação. Tem um grande display de 5.8 polegadas com uma resolução de 2436x1125 pixel. As funcionalidades oferecidas pelo Apple iPhone X são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 256 GB mas sem a possibilidade de expansão. ',
    minimum_price: 3000,
    maximum_price: 4000,
    product_type: 1,
    status: 1,
    brand_id: 2)

Product.find_or_create_by!(name: 'Iphone 7 Plus', 
    description: 'O Apple iPhone 7 Plus é um smartphone iOS avançado e abrangente em todos os pontos de vista com algumas características excelentes. Tem uma grande tela de 5.5 polegadas com uma resolução de 1920x1080 pixels. As funcionalidades oferecidas pelo Apple iPhone 7 Plus são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a memória interna de 32 GB mas sem a possibilidade de expansão. ',
    minimum_price: 1500,
    maximum_price: 3000,
    product_type: 1,
    status: 1,
    brand_id: 2)

# MOTOROLA
Product.find_or_create_by!(name: 'Moto g8', 
    description: 'Tudo novo para você não perder nada neste Smartphone da Motorola. Seu processador Qualcomm Snapdragon 665 é Octa-Core e junto dos 4GB de memória RAM entregam uma capacidade de resposta extremamente rápida, isso significa menos tempo de carregamento dos apps e mais tempo de diversão. Tem 64GB de armazenamento interno, tornando-o ideal para guardar fotos, músicas e vídeos. Se ainda achar pouco, utilize um cartão MicroSD de até 512GB para expandir essa capacidade. Sua tela de 6,4" com resolução HD+ traz valorização ao seu conteúdo favorito e com uma riqueza de detalhes impressionante. O conjunto de 03 câmeras e foco a laser na traseira do Moto G8 fazem questão de tirar fotos nítidas e com uma qualidade incrível. Realize suas selfies e compartilhe nas redes sociais com a câmera frontal de 8MP. A cor Azul Capri ressalta seu design premium e a beleza deste celular. Deixe suas informações mais seguras com o sensor de impressão digital. Permaneça conectado na internet com a tecnologia 4G em um aparelho dual chip. Não se preocupe com a bateria, pois são 4000mAh, além de possuir carregamento rápido de 18W.',
    launch_date: '05/03/2020'.to_date,
    minimum_price: 600,
    maximum_price: 1000,
    product_type: 1,
    status: 1,
    brand_id: 3)

Product.find_or_create_by!(name: 'Moto g7', 
    description: 'Embarque em uma experiência imersiva com a ampla tela do G7. O design sofisticado complementa o formato 19:9, para você aproveitar estilo e performance.',
    minimum_price: 500,
    maximum_price: 1000,
    product_type: 1,
    status: 1,
    brand_id: 3)

Product.find_or_create_by!(name: 'Moto Edge', 
    description: 'Tenha um smartphone em suas mãos que irá mudar seus conceitos! O Motorola edge permite que você capture tudo, pois oferece um conjunto de 04 câmeras na traseira, deixando que sejam feitas desde fotos em alta resolução até fotos nítidas em situações de pouca luz. Faça selfies ainda melhores para compartilhar nas redes sociais, através da câmera de frontal de 25MP. Sua tela de 6,7" pOLED conta com resolução FHD+ para garantir uma visualização de tirar o fôlego ao assistir seus conteúdos favoritos, além de permitir uma navegação mais suave devido a taxa de atualização da tela de 90Hz. Tem 128GB de armazenamento interno, ele é ideal para guardar suas fotos, músicas ou vídeos! E se mesmo assim achar pouco, você pode usar um cartão MicroSD de até 1TB para aumentar essa capacidade. Este produto tem um processador Snapdragon765 Octa-Core e 6GB de memória RAM, que responde de maneira instantânea, desde navegação na Internet à jogos e vídeos. A cor Solar Black destaca o design do smartphone, garantindo maior elegância em suas mãos. Fique sempre conectado com a tecnologia 5G em um aparelho dual chip! Tenha bateria para o dia todo, afinal estamos falando de 4500mAh, além de contar com carregamento TurboPower de 15W. Esse celular entrega proteção personalizada com leitor de impressão digital, deixando seus conteúdos e dados confidenciais protegidos. O Motorola edge surpreende a todos de forma inconfundível.',
    minimum_price: 2500,
    maximum_price: 3500,
    product_type: 1,
    status: 1,
    brand_id: 3)

Product.find_or_create_by!(name: 'Moto E7 Plus', 
    description: 'O Motorola Moto E7 Plus é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.5 polegadas com uma resolução de 1600x720 pixel que não é das mais elevadas. Sobre as características deste Motorola Moto E7 Plus na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 3)

Product.find_or_create_by!(name: 'Moto G9 Play', 
    description: 'O Motorola Moto G9 Play é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.5 polegadas com uma resolução de 1600x720 pixel que não é das mais elevadas. Sobre as características deste Motorola Moto G9 Play na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 3)

Product.find_or_create_by!(name: 'Moto X Play', 
    description: 'O Motorola Moto X Play é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.5 polegadas com uma boa resolução de 1920x1080 pixels. Sobre as características deste Motorola Moto X Play na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 3)

# LG

Product.find_or_create_by!(name: 'K40s', 
    description: 'O LG K40s é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Surpreendente é sua tela Touchscreen de 6.1 polegadas, que coloca esse LG no topo de sua categoria. A resolução também é alta: 1520x720 pixel. Quanto às funções, no LG K40s realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1000,
    product_type: 1,
    status: 1,
    brand_id: 4)

Product.find_or_create_by!(name: 'K12 Max', 
    description: 'O LG K12 Max é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Surpreendente é sua tela Touchscreen de 6.26 polegadas, que coloca esse LG no topo de sua categoria. A resolução também é alta: 1520x720 pixel. Quanto às funções, no LG K12 Max realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 4)

Product.find_or_create_by!(name: 'K50s', 
    description: 'O LG K50s é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Surpreendente é sua tela Touchscreen de 6.5 polegadas, que coloca esse LG no topo de sua categoria. A resolução também é alta: 1520x720 pixel. Quanto às funções, no LG K50s realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1000,
    product_type: 1,
    status: 1,
    brand_id: 4)

Product.find_or_create_by!(name: 'Q6', 
    description: 'O LG Q6 é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.5 polegadas com uma boa resolução de 2160x1080 pixels. Sobre as características deste LG Q6 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, TV e bluetooth. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 4)

# Xiaomi

Product.find_or_create_by!(name: 'Mi 8 Lite', 
    description: 'O Xiaomi Mi 8 Lite é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.26 polegadas com uma resolução de 2280x1080 pixel. Sobre as características deste Xiaomi Mi 8 Lite na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, videoconferência e bluetooth. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 1000,
    maximum_price: 3000,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Mi 9T', 
    description: 'O Xiaomi Mi 9T é um smartphone Android avançado e abrangente em todos os pontos de vista com algumas características excelentes. Tem uma grande tela de 6.39 polegadas com uma resolução de 2340x1080 pixels. As funcionalidades oferecidas pelo Xiaomi Mi 9T são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 128 GB mas sem a possibilidade de expansão. ',
    minimum_price: 1500,
    maximum_price: 3000,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Mi 9', 
    description: 'O Xiaomi MI 9 é, sem dúvida, um dos smartphones Android mais avançados e abrangentes disponíveis no mercado, graças ao seu rico equipamento e recursos multimídia avançados. Tem um grande display de 6.39 polegadas com uma resolução de 2340x1080 pixel. As funcionalidades oferecidas pelo Xiaomi MI 9 são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 64 GB mas sem a possibilidade de expansão. ',
    minimum_price: 1500,
    maximum_price: 3000,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Mi A3', 
    description: 'O Xiaomi Mi A3 é um celular Touchscreen de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.08 polegadas com uma resolução de 1560x720 pixel que não é das mais elevadas. Sobre as características deste Xiaomi Mi A3 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, videoconferência e bluetooth. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 1000,
    maximum_price: 2500,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Redmi Note 6 Pro', 
    description: 'O Xiaomi Redmi Note 6 Pro é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.26 polegadas com uma resolução de 2280x1080 pixel. Sobre as características deste Xiaomi Redmi Note 6 Pro na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 1000,
    maximum_price: 3500,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Mi Note 10', 
    description: 'O Xiaomi Mi Note 10 é, sem dúvida, um dos smartphones Android mais avançados e abrangentes disponíveis no mercado, graças ao seu rico equipamento e recursos multimídia avançados. Tem um grande display de 6.47 polegadas com uma resolução de 2340x1080 pixel. As funcionalidades oferecidas pelo Xiaomi Mi Note 10 são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 128 GB mas sem a possibilidade de expansão. ',
    minimum_price: 2000,
    maximum_price: 3500,
    product_type: 1,
    status: 1,
    brand_id: 5)

Product.find_or_create_by!(name: 'Mi 9 Lite', 
    description: 'O Xiaomi Mi 9 Lite é um smartphone Android avançado e abrangente em todos os pontos de vista com algumas características excelentes. Tem uma grande tela de 6.39 polegadas com uma resolução de 2340x1080 pixels. As funcionalidades oferecidas pelo Xiaomi Mi 9 Lite são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 128 GB com a possibilidade de expansão. ',
    minimum_price: 1500,
    maximum_price: 2700,
    product_type: 1,
    status: 1,
    brand_id: 5)

# Asus

Product.find_or_create_by!(name: 'Zenfone 3 Max (tela 5.5)', 
    description: 'O Asus Zenfone 3 Max (tela 5.5) 2GB RAM é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.5 polegadas com uma boa resolução de 1920x1080 pixels. Sobre as características deste Asus Zenfone 3 Max (tela 5.5) 2GB RAM na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'Zenfone Max (M3)', 
    description: 'O Asus Zenfone Max (M3) é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Tem uma enorme tela Touchscreen de 5.5 polegadas com uma resolução de 1440x720 pixel. Quanto às funções, no Asus Zenfone Max (M3) realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone Live (L1)', 
    description: 'O Asus ZenFone Live (L1) é um smartphone Android mediano, ideal para quem não tem muitas exigências mas não abre mão de um bom display touchscreen. As funções oferecidas pelo Asus ZenFone Live (L1) são mais ou menos as mesmas oferecidas em outros dispositivos avançados, começando com a conectividade Wi-fi e GPS. Tem leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 16 GB com a possibilidade de expansão. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone 3', 
    description: 'O Asus ZenFone 3 é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.2 polegadas com uma boa resolução de 1920x1080 pixels. Sobre as características deste Asus ZenFone 3 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a memória interna de 16 GB com a possibilidade de expansão. ',
    minimum_price: 300,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone 3 (tela 5.5)', 
    description: 'O Asus ZenFone 3 é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.2 polegadas com uma boa resolução de 1920x1080 pixels. Sobre as características deste Asus ZenFone 3 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a memória interna de 16 GB com a possibilidade de expansão. ',
    minimum_price: 300,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone Max Shot', 
    description: 'O Asus ZenFone Max Shot é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma enorme tela Touchscreen de 6.26 polegadas com uma resolução de 2280x1080 pixel. Sobre as características deste Asus ZenFone Max Shot na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 32 GB com a possibilidade de expansão. ',
    minimum_price: 500,
    maximum_price: 1500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone 4', 
    description: 'O Asus ZenFone 4 é um smartphone Android de bom nível, ótimo para fotos, que pode satisfazer até o mais exigente dos usuários. Tem uma grande tela Touchscreen de 5.5 polegadas com uma boa resolução de 1920x1080 pixels. Sobre as características deste Asus ZenFone 4 na verdade não falta nada. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet, além de conectividade Wi-fi e GPS. Tem também leitor multimídia, rádio, videoconferência e bluetooth. Enfatizamos a boa memória interna de 64 GB com a possibilidade de expansão. ',
    minimum_price: 1000,
    maximum_price: 2500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone 6', 
    description: 'O Asus ZenFone 6 é, sem dúvida, um dos smartphones Android mais avançados e abrangentes disponíveis no mercado, graças ao seu rico equipamento e recursos multimídia avançados. Tem um grande display de 6.4 polegadas com uma resolução de 2340x1080 pixel. As funcionalidades oferecidas pelo Asus ZenFone 6 são muitas e inovadoras. Começando pelo LTE 4G que permite a transferência de dados e excelente navegação na internet. Enfatizamos a excelente memória interna de 64 GB com a possibilidade de expansão. O Asus ZenFone 6 é um produto com poucos concorrentes em termos de multimídia graças à câmera de 48 megapixels que permite ao Asus ZenFone 6 tirar fotos fantásticas com uma resolução de 8000x6000 pixels e gravar vídeos em 4K a espantosa resolução de 3840x2160 pixels. ',
    minimum_price: 1500,
    maximum_price: 3500,
    product_type: 1,
    status: 1,
    brand_id: 6)

Product.find_or_create_by!(name: 'ZenFone Live (L2)', 
    description: 'O Asus ZenFone Live (L2) é um smartphone Android completo, que não tem muito a invejar aos mais avançados dispositivos. Tem uma enorme tela Touchscreen de 5.5 polegadas com uma resolução de 1440x720 pixel. Quanto às funções, no Asus ZenFone Live (L2) realmente não falta nada. Começando pelo conectividade Wi-fi e GPS. A transferência de dados e navegação web sao fornecidas pela rede UMTS, mas não suporta tecnologias mais recentes, tais como HSDPA. Enfatizamos a memória interna de 16 GB com a possibilidade de expansão. ',
    minimum_price: 300,
    maximum_price: 1000,
    product_type: 1,
    status: 1,
    brand_id: 6)

#Cliente
if !Customer.where(email: 'joaovitor@gmail.com').present?
	customer_1 = Customer.find_or_create_by!(email: 'joaovitor@gmail.com', name: 'João Vitor Silva Prado', birth_date: '20/12/1998'.to_date, cpf: '08191215098', kind: 'P') do |user|
	  user.password = "password"
	  user
	end.id
end
customer_1 = Customer.where(email: 'joaovitor@gmail.com').first.id

if !Customer.where(email: 'calebe_edpi@gmail.com').present?
	customer_2 = Customer.find_or_create_by!(email: 'calebe_edpi@gmail.com', name: 'Calebe Eduardo Pires', birth_date: '02/07/1940'.to_date, cpf: '63990885189', kind: 'P') do |user|
	  user.password = "password"
	  user
	end.id
end
customer_2 = Customer.where(email: 'calebe_edpi@gmail.com').first.id

# Anúncios
listing_1 = Listing.find_or_create_by!(description: 'Produto novo ainda na caixa, sem uso nenhum.', product_id: 1, product_quality_id: 1, customer_id: customer_1, price: 3000)
listing_25 = Listing.find_or_create_by!(description: 'Produto novo ainda na caixa, sem uso nenhum.', product_id: 1, product_quality_id: 1, customer_id: customer_2, price: 3050, discount: 10)
listing_2 = Listing.find_or_create_by!(description: 'Produto novo ainda na caixa, sem uso nenhum.', product_id: 10, product_quality_id: 1, customer_id: customer_2, price: 2000)
listing_3 = Listing.find_or_create_by!(description: 'Produto novo ainda na caixa..', product_id: 8, product_quality_id: 1, customer_id: customer_1, price: 2500)
listing_26 = Listing.find_or_create_by!(description: 'Produto novo ainda na caixa..', product_id: 8, product_quality_id: 1, customer_id: customer_2, price: 2510, discount: 5)
listing_4 = Listing.find_or_create_by!(description: 'Produto novo não usado, não gostei. sem uso nenhum.', product_id: 5, product_quality_id: 1, customer_id: customer_2, price: 3231)
listing_5 = Listing.find_or_create_by!(description: 'Produto novo não usado, não gostei. sem uso nenhum.', product_id: 1, product_quality_id: 1, customer_id: customer_1, price: 3000)
listing_6 = Listing.find_or_create_by!(description: 'Produto novo não usado, não gostei. sem uso nenhum.', product_id: 11, product_quality_id: 1, customer_id: customer_2, price: 1500)
listing_7 = Listing.find_or_create_by!(description: 'Não gostei do produto e estou vendedo, possui apenas pequenas marcas', product_id: 6, product_quality_id: 2, customer_id: customer_1, price: 2670)
listing_24 = Listing.find_or_create_by!(description: 'Não gostei do produto e estou vendedo, possui apenas pequenas marcas', product_id: 6, product_quality_id: 2, customer_id: customer_1, price: 2670, discount: 15)
listing_8 = Listing.find_or_create_by!(description: 'Não gostei do produto e estou vendedo, possui apenas pequenas marcas', product_id: 27, product_quality_id: 2, customer_id: customer_1, price: 1560)
listing_9 = Listing.find_or_create_by!(description: 'Estou vendendo por necessidade, o produto está tudo ok sem problemas nem arranhões.', product_id: 10, product_quality_id: 2, customer_id: customer_2, price: 1761)
listing_10 = Listing.find_or_create_by!(description: 'Estou vendendo por necessidade, o produto está tudo ok sem problemas nem arranhões.', product_id: 5, product_quality_id: 2, customer_id: customer_1, price: 2400)
listing_11 = Listing.find_or_create_by!(description: 'Estou vendendo por necessidade de venda, o produto está tudo sem problemas nem arranhões.', product_id: 41, product_quality_id: 2, customer_id: customer_1, price: 3100)
listing_12 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 1, product_quality_id: 3, customer_id: customer_2, price: 2600)
listing_13 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 10, product_quality_id: 3, customer_id: customer_1, price: 1800)
listing_27 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 10, product_quality_id: 3, customer_id: customer_2, price: 1860, discount: 10)
listing_14 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 30, product_quality_id: 3, customer_id: customer_2, price: 2100)
listing_15 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 41, product_quality_id: 3, customer_id: customer_1, price: 2600)
listing_28 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 41, product_quality_id: 3, customer_id: customer_2, price: 2700, discount: 15)
listing_16 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 27, product_quality_id: 3, customer_id: customer_2, price: 2300)
listing_29 = Listing.find_or_create_by!(description: 'Produto usado normalmente sem problemas de funcionamento e bem conservado. Pequenos arranhões aparentes.', product_id: 27, product_quality_id: 3, customer_id: customer_1, price: 2380, discount: 20)
listing_17 = Listing.find_or_create_by!(description: 'Produto com a câmera sem funcionar e sem o botão de home.', product_id: 1, product_quality_id: 5, customer_id: customer_1, price: 1000)
listing_18 = Listing.find_or_create_by!(description: 'Produto com a câmera sem funcionar.', product_id: 10, product_quality_id: 5, customer_id: customer_2, price: 580)
listing_19 = Listing.find_or_create_by!(description: 'Produto com a tela trincada mas funcional.', product_id: 11, product_quality_id: 4, customer_id: customer_1, price: 880)
listing_20 = Listing.find_or_create_by!(description: 'Produto usado poucas vezes, um pequeno arranhão na lateral, porém muito bem conservado', product_id: 8, product_quality_id: 2, customer_id: customer_2, price: 1580)
listing_30 = Listing.find_or_create_by!(description: 'Produto usado poucas vezes, um pequeno arranhão na lateral, porém muito bem conservado', product_id: 8, product_quality_id: 2, customer_id: customer_2, price: 1670, discount: 20)
listing_21 = Listing.find_or_create_by!(description: 'Produto usado poucas vezes, um pequeno arranhão na lateral, porém muito bem conservado', product_id: 30, product_quality_id: 2, customer_id: customer_2, price: 2500)
listing_22 = Listing.find_or_create_by!(description: 'Não gostei do produto e estou vendedo, possui apenas pequenas marcas', product_id: 30, product_quality_id: 2, customer_id: customer_1, price: 2670)
listing_23 = Listing.find_or_create_by!(description: 'Não gostei do produto e estou vendedo, possui apenas pequenas marcas', product_id: 30, product_quality_id: 2, customer_id: customer_2, price: 2500, discount: 10)

# Imagens
# Produto 1
Image.find_or_create_by!(listing_id: listing_1.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_1.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_1.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_6SZ.jpg')
Image.find_or_create_by!(listing_id: listing_5.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217475_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_25.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217475_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_5.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217475_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_25.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217475_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_12.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_12.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_12.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_6SZ.jpg')
Image.find_or_create_by!(listing_id: listing_17.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_26.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_17.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_26.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_7SZ.jpg')
Image.find_or_create_by!(listing_id: listing_17.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_6SZ.jpg')
Image.find_or_create_by!(listing_id: listing_26.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/3/134217352_6SZ.jpg')

#Produto 10
Image.find_or_create_by!(listing_id: listing_2.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_2.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_9.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_9.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_13.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_27.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_13.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_27.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_18.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_18.id, url: 'https://images-americanas.b2w.io/produtos/01/00/offers/01/00/item/134061/7/134061749_4SZ.jpg')

#Produto 8
Image.find_or_create_by!(listing_id: listing_3.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1691398/2/1691398221_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_3.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1691398/2/1691398221_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_3.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1691398/2/1691398221_3SZ.jpg')
Image.find_or_create_by!(listing_id: listing_20.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_3SZ.jpg')
Image.find_or_create_by!(listing_id: listing_30.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_3SZ.jpg')
Image.find_or_create_by!(listing_id: listing_20.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_6SZ.jpg')
Image.find_or_create_by!(listing_id: listing_30.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_6SZ.jpg')
Image.find_or_create_by!(listing_id: listing_20.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_2SZ.jpg')
Image.find_or_create_by!(listing_id: listing_30.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/1669977/4/1669977420_2SZ.jpg')

#Produto 5
Image.find_or_create_by!(listing_id: listing_4.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217459_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_4.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134217/4/134217459_5SZ.jpg')
Image.find_or_create_by!(listing_id: listing_10.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134218/1/134218195_4SZ.jpg')
Image.find_or_create_by!(listing_id: listing_10.id, url: 'https://images-americanas.b2w.io/produtos/01/00/img/134218/1/134218195_6SZ.jpg')

#Produto 27
Image.find_or_create_by!(listing_id: listing_8.id, url: 'https://images-na.ssl-images-amazon.com/images/I/618l6jURCtL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_8.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51FpB3qGCCL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_16.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51pmIo6shDL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_29.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51pmIo6shDL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_16.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51yUhHe164L._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_29.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51yUhHe164L._AC_SL1500_.jpg')

#Produto 11
Image.find_or_create_by!(listing_id: listing_6.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51yuyJCINxL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_6.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51Pk%2BSqBbcL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_19.id, url: 'https://img.olx.com.br/images/91/912036196084912.jpg')

#Produto 6
Image.find_or_create_by!(listing_id: listing_7.id, url: 'https://images-na.ssl-images-amazon.com/images/I/710RIZ6pxPL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_24.id, url: 'https://images-na.ssl-images-amazon.com/images/I/710RIZ6pxPL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_7.id, url: 'https://images-na.ssl-images-amazon.com/images/I/61jUsPw8NaL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_24.id, url: 'https://images-na.ssl-images-amazon.com/images/I/61jUsPw8NaL._AC_SL1500_.jpg')

# Produto 41
Image.find_or_create_by!(listing_id: listing_11.id, url: 'https://images-na.ssl-images-amazon.com/images/I/41NblXzVM9L._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_11.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51bMXempYTL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_15.id, url: 'https://images-na.ssl-images-amazon.com/images/I/61CpfL2EWwL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_28.id, url: 'https://images-na.ssl-images-amazon.com/images/I/61CpfL2EWwL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_15.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51SGQLH2qrL._AC_SL1000_.jpg')
Image.find_or_create_by!(listing_id: listing_28.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51SGQLH2qrL._AC_SL1000_.jpg')

#Produto 30
Image.find_or_create_by!(listing_id: listing_14.id, url: 'https://images-na.ssl-images-amazon.com/images/I/7107er0dj6L._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_14.id, url: 'https://images-na.ssl-images-amazon.com/images/I/71VbfJ2GQBL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_21.id, url: 'https://images-na.ssl-images-amazon.com/images/I/81PledBMMlL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_21.id, url: 'https://images-na.ssl-images-amazon.com/images/I/71IzETzpQgL._AC_SL1500_.jpg')
Image.find_or_create_by!(listing_id: listing_22.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51posBEXwML._AC_.jpg')
Image.find_or_create_by!(listing_id: listing_23.id, url: 'https://images-na.ssl-images-amazon.com/images/I/51posBEXwML._AC_.jpg')
Image.find_or_create_by!(listing_id: listing_22.id, url: 'https://images-na.ssl-images-amazon.com/images/I/11FSaHYbKVL._AC_.jpg')
Image.find_or_create_by!(listing_id: listing_23.id, url: 'https://images-na.ssl-images-amazon.com/images/I/11FSaHYbKVL._AC_.jpg')